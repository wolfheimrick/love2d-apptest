character = {}

character.posx = 2
character.posy = 2
character.img = love.graphics.newImage("player.png")
character.dir = {
   north = false,
   south = false,
   east = false,
   west = false
}

function character:move(dir)
   if dir == "down" then
      self.posy = self.posy + 1
   elseif dir == "up" then
      self.posy = self.posy - 1
   elseif dir == "right" then
      self.posx = self.posx + 1
   elseif dir == "left" then
      self.posx = self.posx - 1
   end
end

function character:checkaround(m)
   self.dir.north = m:check(self.posx, self.posy - 1)
   self.dir.south = m:check(self.posx, self.posy + 1)
   self.dir.east = m:check(self.posx + 1, self.posy)
   self.dir.west = m:check(self.posx - 1, self.posy)
end
 
function character:new(o)
   o = o or {}
   setmetatable(o, self)
   self.__index = self
   return o
end

return character