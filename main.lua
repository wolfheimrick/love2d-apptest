local gridmap = require 'gridmap'
local player = require 'character'

map = gridmap:new()
player = character:new()

wall = {
   img = love.graphics.newImage("env_wall.png")
}

empty = {
   img = love.graphics.newImage("env_empty.png")
}

function drawmap()
   for j, line in pairs(map.data) do
      strline = ""
      for i, e in pairs(line) do
         strline = strline .. e
         if e == 0 then
            love.graphics.draw(empty.img, i*64-64, j*64-64)
         elseif e == 1 then
            love.graphics.draw(wall.img, i*64-64, j*64-64)
         end
      end
   end
end

 function love.draw()
   love.graphics.clear()
   drawmap()
   love.graphics.draw(player.img, player.posx*64-64, player.posy*64-64)
 end

 function love.keypressed(key)
   if key == 'escape' then love.event.quit() end
   player:checkaround(map)
   if key == "down" and player.dir.south == true then
      player:move(key)
   elseif key == "up" and player.dir.north == true then
      player:move(key)
   elseif key == "right" and player.dir.east == true then
      player:move(key)
   elseif key == "left" and player.dir.west == true then
      player:move(key)
   end
 end
